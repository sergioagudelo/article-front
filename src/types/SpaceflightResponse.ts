export type SpaceflightResponse = {
  imageUrl: string;
  title: string;
  url: string;
};
