import React from "react";
import { SpaceflightResponse } from "../types/SpaceflightResponse";

interface SpaceflightItem {
  data: SpaceflightResponse;
  index: number;
}

export const SpaceFlightItem = (props: SpaceflightItem) => {
  return (
    <React.Fragment key={props.index}>
      <a href={props.data.url} target="_blank" rel="noopener noreferrer">
        {props.index} - {props.data.title}
      </a>
      <img
        src={props.data.imageUrl}
        alt={props.data.title}
        style={{ maxWidth: 300 }}
      />
      <br />
    </React.Fragment>
  );
};
