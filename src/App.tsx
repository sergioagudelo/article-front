import "./App.css";

import { SpaceFlightList } from "./components/SpaceFlightList";

function App() {
  return (
    <div>
      <SpaceFlightList />
    </div>
  );
}

export default App;
